package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers.isEmpty()) throw new CannotBuildPyramidException();
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int numOfLines = 0;
        while (inputNumbers.size() >= (numOfLines * (numOfLines + 1) / 2)) {  // (numOfLines * (numOfLines + 1) / 2) counts,
                                                                              // how many numbers should be in the pyramid
            if (numOfLines >= inputNumbers.size())
                throw new CannotBuildPyramidException();
            if (inputNumbers.size() == (numOfLines * (numOfLines + 1) / 2)) // if there are as many numbers in input as required for a pyramid
                return formPyramid(inputNumbers, numOfLines);
            ++numOfLines;
        }
        throw new CannotBuildPyramidException();
    }

    int[][] formPyramid(List<Integer> inputNumbers, int numOfLines) {
        Collections.sort(inputNumbers);
        int[][] pyramid = new int[numOfLines][numOfLines * 2 - 1];

        for (int i = 0; i < pyramid.length; i++)
            for (int j = 0; j < pyramid[i].length; j++)
                pyramid[i][j] = 0;

        int currentNumToInsert = 0;
        for (int i = 0; i < pyramid.length; i++) {
            int j = numOfLines - i - 1;
            int amountOfNumsInRow = i + 1;
            while (amountOfNumsInRow > 0) {
                pyramid[i][j] = inputNumbers.get(currentNumToInsert);
                ++currentNumToInsert;
                --amountOfNumsInRow;
                j += 2;
            }
        }
        return  pyramid;
    }
}
