package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calculator {

    private Stack<Character> operatorStack;
    private Stack<Double> valueStack;
    private boolean error;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty())
            return null;

        operatorStack = new Stack<Character>();
        valueStack = new Stack<Double>();
        error = false;

        return processInput(parseInput(statement));
    }

    private List<String> parseInput(String input) {
        List<String> tokens = new ArrayList<>();

        for (int n = 0; n < input.length(); n++) {
            if ( input.charAt(n) >= '0' && input.charAt(n) <= '9') {
                StringBuilder builder = new StringBuilder();
                while ( (n < input.length())  && ((input.charAt(n) >= '0' && input.charAt(n) <= '9')
                        || input.charAt(n) == '.') )
                {
                    builder.append(input.charAt(n));
                    ++n;
                }
                tokens.add(builder.toString());
                if (n == input.length())
                    break;
                if(input.charAt(n) < '0' || input.charAt(n) > '9')
                    --n;
            }
            else if (input.charAt(n) == '(' || input.charAt(n) == ')'
                    || input.charAt(n) == '-' || input.charAt(n) == '+'
                    || input.charAt(n) == '*' || input.charAt(n) == '/')
            {
                tokens.add("" + input.charAt(n));
            }
        }
        return tokens;
    }

    private void processOperator(char t) {
        double a, b;
        if (valueStack.empty()) {
//      Expression error
            error = true;
            return;
        } else {
            b = valueStack.peek();
            valueStack.pop();
        }
        if (valueStack.empty()) {
//           Expression error
            error = true;
            return;
        } else {
            a = valueStack.peek();
            valueStack.pop();
        }
        double r = 0;
        if (t == '+') {
            r = a + b;
        } else if (t == '-') {
            r = a - b;
        } else if (t == '*') {
            r = a * b;
        } else if(t == '/') {
            if (b == 0) error = true;
            r = a / b;
        } else {
//            Operator error
            error = true;
        }
        valueStack.push(r);
    }

    private boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    public String processInput( List<String> tokens) {

        // Main loop - process all input tokens
        for (String nextToken : tokens) {
            char ch = nextToken.charAt(0);
            if ((ch >= '0' && ch <= '9') || (ch == '-' && nextToken.length() > 1)) {
                try {
                    Double.parseDouble(nextToken);
                } catch (Exception NumberFormatException) {
                    return null;
                }
                double value = Double.parseDouble(nextToken);
                valueStack.push(value);
            } else if (isOperator(ch)) {
                if (operatorStack.empty() || getPrecedence(ch) > getPrecedence(operatorStack.peek())) {
                    operatorStack.push(ch);
                } else {
                    while (!operatorStack.empty() && getPrecedence(ch) <= getPrecedence(operatorStack.peek())) {
                        char toProcess = operatorStack.peek();
                        operatorStack.pop();
                        processOperator(toProcess);
                    }
                    operatorStack.push(ch);
                }
            } else if (ch == '(') {
                operatorStack.push(ch);
            } else if (ch == ')') {
                while (!operatorStack.empty() && isOperator(operatorStack.peek())) {
                    char toProcess = operatorStack.peek();
                    operatorStack.pop();
                    processOperator(toProcess);
                }
                if (!operatorStack.empty() && operatorStack.peek() == '(') {
                    operatorStack.pop();
                } else {
                    error = true;
                }
            }
        }
        // Empty out the operator stack at the end of the input
        while (!operatorStack.empty() && isOperator(operatorStack.peek())) {
            char toProcess = operatorStack.peek();
            operatorStack.pop();
            processOperator(toProcess);
        }
        // Return the result if no error has been seen
        if (!error) {
            double result = valueStack.peek();
            valueStack.pop();
            if (!operatorStack.empty() || !valueStack.empty()) {
                return null;
            } else {
                result = Math.round(result * 10000.0) / 10000.0;
                return result % 1 == 0 ? String.valueOf((int) result)
                        : String.valueOf(result);
            }
        }
        return null;
    }

    private int getPrecedence(char ch) {
        if (ch == '+' || ch == '-') {
            return 1;
        }
        if (ch == '*' || ch == '/') {
            return 2;
        }
        return 0;
    }
}
