package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (x.isEmpty())
            return true;
        if (y.isEmpty())
            return false;

        int currentXIdx = 0;
        Object tokenToFind = x.get(currentXIdx);

        for(Object currentY: y){
            if (tokenToFind == currentY){
                if (currentXIdx == x.size() - 1)
                    return true;
                ++currentXIdx;
                tokenToFind = x.get(currentXIdx);
            }
        }
        return false;
    }
}
